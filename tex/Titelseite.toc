\babel@toc {ngerman}{}
\contentsline {section}{\numberline {1}Einleitung}{3}{section.1}%
\contentsline {section}{\numberline {2}Über das Unternehmen}{4}{section.2}%
\contentsline {subsection}{\numberline {2.1}Anlagen}{4}{subsection.2.1}%
\contentsline {subsection}{\numberline {2.2}Geschichte}{4}{subsection.2.2}%
\contentsline {section}{\numberline {3}Problemstellung}{6}{section.3}%
\contentsline {subsection}{\numberline {3.1}Allgemeine Problemstellung}{6}{subsection.3.1}%
\contentsline {subsection}{\numberline {3.2}Aufgaben}{8}{subsection.3.2}%
\contentsline {subsection}{\numberline {3.3}Weitere Problemstellungen}{9}{subsection.3.3}%
\contentsline {section}{\numberline {4}Problemlösungen}{10}{section.4}%
\contentsline {subsection}{\numberline {4.1}Drucksensoren und Signale}{10}{subsection.4.1}%
\contentsline {subsection}{\numberline {4.2}Isolierte Abtastung}{10}{subsection.4.2}%
\contentsline {subsection}{\numberline {4.3}SPS Kommunikation}{13}{subsection.4.3}%
\contentsline {subsubsection}{\numberline {4.3.1}PG/OP}{13}{subsubsection.4.3.1}%
\contentsline {subsection}{\numberline {4.4}Profibus}{14}{subsection.4.4}%
\contentsline {subsection}{\numberline {4.5}CAN}{14}{subsection.4.5}%
\contentsline {subsection}{\numberline {4.6}TCP}{14}{subsection.4.6}%
\contentsline {subsection}{\numberline {4.7}Wahl des Mikrocontrollers}{15}{subsection.4.7}%
\contentsline {subsubsection}{\numberline {4.7.1}Mikrocontroller/Prozessor}{15}{subsubsection.4.7.1}%
\contentsline {subsubsection}{\numberline {4.7.2}Peripherie}{16}{subsubsection.4.7.2}%
\contentsline {subsubsection}{\numberline {4.7.3}Begründung der Wahl}{16}{subsubsection.4.7.3}%
\contentsline {subsection}{\numberline {4.8}Peak Detektion}{18}{subsection.4.8}%
\contentsline {subsection}{\numberline {4.9}Algorithmus}{19}{subsection.4.9}%
\contentsline {subsubsection}{\numberline {4.9.1}Erstellung des geglätteten Modells}{20}{subsubsection.4.9.1}%
\contentsline {paragraph}{\numberline {4.9.1.1}Gleitende Standardabweichung (moving standard deviation)}{20}{paragraph.4.9.1.1}%
\contentsline {paragraph}{\numberline {4.9.1.2}Gleitender Mittelwert (moving average)}{20}{paragraph.4.9.1.2}%
\contentsline {paragraph}{\numberline {4.9.1.3}Vollständiges Modell}{21}{paragraph.4.9.1.3}%
\contentsline {subsubsection}{\numberline {4.9.2}Peaks finden und filtern}{22}{subsubsection.4.9.2}%
\contentsline {paragraph}{\numberline {4.9.2.1}Lokale Maxima und Minima im Modell finden}{22}{paragraph.4.9.2.1}%
\contentsline {paragraph}{\numberline {4.9.2.2}Peaks Filtern und im echten Spektrum finden}{24}{paragraph.4.9.2.2}%
\contentsline {subsection}{\numberline {4.10}Ergebnisse des Algorithmus}{25}{subsection.4.10}%
\contentsline {section}{\numberline {5}Leiterplatte und Gehäuse}{26}{section.5}%
\contentsline {section}{\numberline {6}Tests}{28}{section.6}%
\contentsline {section}{\numberline {7}Resümee}{30}{section.7}%
\contentsline {section}{Quellen}{31}{section*.11}%
\contentsline {section}{Unterschrift}{32}{section*.11}%
