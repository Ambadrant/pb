#!/bin/bash
mv Titelseite.pdf pb.pdf
mv *.tex tex/
mv *.blg tex/
mv *.log tex/
mv *.out tex/
mv *.toc tex/
mv *.aux tex/
mv *.bbl tex/
mv *.bib tex/
